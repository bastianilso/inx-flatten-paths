# inx-flatten-clip-paths

A pair of Inkscape extensions to flatten clipped paths and paths with
white filler to a single path with similar visual representation.
Useful to e.g. clean up geometry, when using vector graphics in contexts
that don't support path clipping.

''This is alpha software and have barely been tested on wide range of use cases.''

## Basic Usage

This extension takes a selection of paths or a group of paths, and 
attempts to flatten them. "Flatten Clip Paths" releases the path clip
and performs several intersection booleans between the clip object and 
all selected paths. "Flatten Path Fill" performs union booleans from 
bottom and up and performs difference booleans between the geometry and
any white-colored object that appears in the stack. It's useful when 
e.g. white color has been used to mask/hide parts of the vector image.

Clones or bitmap images are silently ignored.

## Installation

Copy all .inx and .py files into the user extensions
directory (see 'Inkscape Preferences > System' for the exact location)
and relaunch Inkscape.

### The extensions will be available as:

**Extensions > Flatten Path:**
- 1 Flatten Clip Path
- 2 Flatten Path Fill

## Source

This is a fork of moini_ink's "inx-pathops" extension, and works with Inkscape 1.0 and higher. 
https://gitlab.com/moini_ink/inx-pathops

## License

GPL-2+
